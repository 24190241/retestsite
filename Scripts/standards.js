function menuOpen() {
    var x = document.getElementById("openBtn");
    var y = document.getElementById("menuLinks");
    if (x.className === "burger") {
        x.className += "_responsive";
        y.className += "_responsive";
    } else {
        x.className = "burger";
        y.className = "menuLinks"
    }
}